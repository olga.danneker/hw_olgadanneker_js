var doc = {
    header: undefined,
    body:undefined,
    footer:undefined,
    date:undefined,
    annex:{
        header:{},
        body:{},
        footer:{},
        date:{}
    },
    addDoc: function(){
        this.header=prompt("Input document header");
        this.body=prompt("Input document body");
        this.footer=prompt("Input document footer");
        this.date=prompt("Input document date");
        this.annex.header=prompt("Input document annex header");
        this.annex.body=prompt("Input document annex body");
        this.annex.footer=prompt("Input document annex footer");
        this.annex.date=prompt("Input document annex date");
    },
    outDoc: function(){
        document.write ("Document header: "+this.header+'<br>');
        document.write ("Document body: "+this.body+'<br>');
        document.write ("Document footer: "+this.footer+'<br>');
        document.write ("Document date: "+this.date+'<br>');
        document.write ("Document Annex header: "+this.annex.header+'<br>');
        document.write ("Document Annex body: " + this.annex.body+'<br>');
        document.write ("Document Annex footer: "+this.annex.footer+'<br>');
        document.write ("Document Annex date: "+this.annex.date+'<br>');
    }
}

doc.addDoc();
doc.outDoc();

