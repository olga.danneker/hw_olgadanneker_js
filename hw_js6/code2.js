function Human(data){
    //методы и свойства уровня экземпляра
    this.name=data[0];
    this.surname=data[1];
    this.age=data[2];
    this.getpass=function(){
        let firstletter=`${this.name[0]}`.toLowerCase()
        return `${firstletter}${this.surname}${this.age}`
    }
}
Human.prototype.generateAditionalContractNum=function(){
    //функция конструктора
    return this.age*2021
}
const data = prompt("Name,Surname,Age").split(',');
const a = new Human(data);
a.getpass();
a.generateAditionalContractNum();